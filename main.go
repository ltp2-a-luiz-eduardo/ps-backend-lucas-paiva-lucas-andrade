package main

import (
    "context"
    "fmt"
    "log"
    "net/http"
    "os"
    "path/filepath"
    "time"

    "go.mongodb.org/mongo-driver/bson"
    "go.mongodb.org/mongo-driver/mongo"
    "go.mongodb.org/mongo-driver/mongo/options"
    "go.mongodb.org/mongo-driver/mongo/readpref"
)

var client *mongo.Client

func connectDB() {
    ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
    defer cancel()
    var err error
    client, err = mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
    if err != nil {
        log.Fatal(err)
    }

    if err := client.Ping(ctx, readpref.Primary()); err != nil {
        log.Fatal(err)
    }

    fmt.Println("Connected to MongoDB!")
}

func getBooksHandler(w http.ResponseWriter, r *http.Request) {
    query := r.URL.Query().Get("query")
    collection := client.Database("your_database").Collection("books")

    var result bson.M
    err := collection.FindOne(context.Background(), bson.M{"name": query}).Decode(&result)
    if err != nil {
        http.Error(w, "Book not found", http.StatusNotFound)
        return
    }

    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(http.StatusOK)
    w.Write([]byte(fmt.Sprintf(`{"name": "%s", "url": "%s"}`, result["name"], result["url"])))
}

func servePDFHandler(w http.ResponseWriter, r *http.Request) {
    pdfPath := filepath.Join("pdfs", filepath.Base(r.URL.Path))
    if _, err := os.Stat(pdfPath); os.IsNotExist(err) {
        http.Error(w, "PDF not found", http.StatusNotFound)
        return
    }
    http.ServeFile(w, r, pdfPath)
}

func serveFrontend() http.Handler {
    staticFiles := http.FileServer(http.Dir("./dist"))
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        if _, err := os.Stat(filepath.Join("./dist", r.URL.Path)); os.IsNotExist(err) {
            http.ServeFile(w, r, "./dist/index.html")
            return
        }
        staticFiles.ServeHTTP(w, r)
    })
}

func main() {
    connectDB()

    mux := http.NewServeMux()
    mux.HandleFunc("/api/books", getBooksHandler)
    mux.HandleFunc("/pdfs/", servePDFHandler)
    mux.Handle("/", serveFrontend())

    server := &http.Server{
        Addr:    ":8080",
        Handler: mux,
    }

    log.Println("Servidor rodando em http://localhost:8080")
    log.Fatal(server.ListenAndServe())
}